<?php

use yii\db\Migration;

/**
 * Class m230222_015400_create_table_posts
 */
class m230222_015400_create_table_posts extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable("posts", [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'title' => $this->string()->notNull(),
            'body' => $this->text()->null(),
            'slug' => $this->string()->notNull(),
            'file' => $this->string()->null()->defaultValue(null)
        ]);

        $this->addForeignKey("user_id_fk", 'posts', ['user_id'], \common\models\User::tableName(), ['id'], 'cascade', 'cascade');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m230222_015400_create_table_posts cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m230222_015400_create_table_posts cannot be reverted.\n";

        return false;
    }
    */
}
