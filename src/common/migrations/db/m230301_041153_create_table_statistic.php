<?php

use yii\db\Migration;

/**
 * Class m230301_041153_create_table_statistic
 */
class m230301_041153_create_table_statistic extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable("statistic", [
            'id' => $this->primaryKey(),
            'event_type' => $this->tinyInteger(2)->notNull(),
            'click_id' => $this->string(30)->notNull(),
            'user_id' => $this->integer()->notNull(),
            'date' => $this->dateTime(),
            'ip' => $this->string(45)->notNull()
        ]);

        $this->createIndex("statistic_click_id_idx", "statistic", ['click_id'], true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable("statistic");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m230301_041153_create_table_statistic cannot be reverted.\n";

        return false;
    }
    */
}
