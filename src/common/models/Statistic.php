<?php

namespace common\models;

use Hidehalo\Nanoid\Client;
use yii\db\Expression;

/**
 * This is the model class for table "statistic".
 *
 * @property int $id
 * @property int $event_type
 * @property string $click_id
 * @property int $user_id
 * @property string|null $date
 * @property string $ip
 */
class Statistic extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'statistic';
    }

    /**
     * @param int $length
     * @return string
     */
    public static function generateUiniqId(int $length = 21): string
    {
        $bytes = openssl_random_pseudo_bytes((int)ceil($length / 2));
        return substr(bin2hex($bytes), 0, $length);
    }

    public static function gen_uuid()
    {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),

            // 16 bits for "time_mid"
            mt_rand(0, 0xffff),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand(0, 0x0fff) | 0x4000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand(0, 0x3fff) | 0x8000,

            // 48 bits for "node"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

    /**
     * @return string
     * @throws \yii\db\Exception
     */
    public static function generateUiniqIdByMysql(): string
    {
        return \Yii::$app->db->createCommand("SELECT uuid();")->queryScalar();
    }

    /**
     * @param int $size
     * @return string
     */
    public static function generateNanoId(int $size = 21): string
    {
        $client = new Client();

        # default random generator
        //echo $client->generateId($size) . PHP_EOL;
        # more safer random generator
        return $client->generateId($size, $mode = Client::MODE_DYNAMIC);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['event_type', 'click_id', 'user_id', 'ip'], 'required'],
            [['event_type', 'user_id'], 'integer'],
            [['date'], 'safe'],
            [['click_id'], 'string', 'max' => 30],
            [['ip'], 'string', 'max' => 45],
            [['click_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'event_type' => 'Event Type',
            'click_id' => 'Click ID',
            'user_id' => 'User ID',
            'date' => 'Date',
            'ip' => 'Ip',
        ];
    }

    /**
     * @param string $campaign
     * @param string $clickId
     * @return bool
     * @throws \Exception
     */
    public static function makeClick(string $campaign, string $clickId, string $ip): bool
    {
        $model = new Statistic();
        $model->click_id = $clickId;
        $model->user_id = 2;
        $model->date = new Expression('NOW()');
        $model->event_type = 1;
        $model->ip = $ip;
        if ($model->save(false) === false) {
            throw new \Exception("Can't save statistic.");
        }
        return true;
    }
}
