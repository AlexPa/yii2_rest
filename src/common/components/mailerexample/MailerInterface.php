<?php

namespace common\components\mailerexample;

interface MailerInterface
{
    public function send();
}