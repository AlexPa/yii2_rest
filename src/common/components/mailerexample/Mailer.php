<?php

namespace common\components\mailerexample;

class Mailer implements MailerInterface
{
    private string $transport = "";

    public function __construct(string $transport)
    {
        $this->transport = $transport;
    }


    public function send()
    {
        return $this->transport;
    }
}