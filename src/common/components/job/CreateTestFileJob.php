<?php

namespace common\components\job;

use common\models\Statistic;
use yii\base\BaseObject;
use yii\queue\JobInterface;

class CreateTestFileJob extends BaseObject implements JobInterface
{
    public string $filename;

    public function __construct($config = [])
    {
        parent::__construct($config);
    }


    public function execute($queue)
    {
        $count = Statistic::find()->count();
        file_put_contents($this->filename, date("Y-m-d H:i:s") . " - " . $count . "</br>", FILE_APPEND);
    }
}