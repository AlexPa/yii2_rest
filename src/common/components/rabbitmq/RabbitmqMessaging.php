<?php

namespace common\components\rabbitmq;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class RabbitmqMessaging implements MessagingInterface
{
    public function __construct(
        private readonly string $host,
        private readonly int    $port,
        private readonly string $user,
        private readonly string $password,
        private readonly string $vhost,
        private string          $exchange,
        private ?string         $queue = null
    )
    {
    }

    /**
     * @return string
     */
    public function getExchange(): string
    {
        return $this->exchange;
    }

    /**
     * @param string $exchange
     */
    public function setExchange(string $exchange): void
    {
        $this->exchange = $exchange;
    }

    /**
     * @return string
     */
    public function getQueue(): string
    {
        return $this->queue;
    }

    /**
     * @param string $queue
     */
    public function setQueue(string $queue): void
    {
        $this->queue = $queue;
    }


    public function send(string $message, string $key): bool
    {
        try {
            $connection = new AMQPStreamConnection($this->host, $this->port, $this->user, $this->password, $this->vhost);
            $channel = $connection->channel();


            $channel->exchange_declare($this->exchange, 'direct');
            if ($this->queue !== null) {
                $channel->queue_declare($this->queue, false, false, false, false);
                $channel->queue_bind($this->queue, $this->exchange, $key);
            }

            $msg = new AMQPMessage($message);
            $channel->basic_publish($msg, $this->exchange, $key);
            $channel->close();
            $connection->close();
        } catch (\Exception $e) {
            \Yii::error($e->getMessage() . $e->getTraceAsString());
        }
        return true;
    }

    public function receive(string $queue, string $key, \Closure $func): void
    {
        $connection = new AMQPStreamConnection($this->host, $this->port, $this->user, $this->password, $this->vhost);
        $channel = $connection->channel();

        $channel->queue_declare($queue, false, false, false, false);
        $callback = function (AMQPMessage $msg) use ($func) {
            $func($msg);
        };

        $channel->basic_consume($queue, '', false, true, false, false, $callback);

        while ($channel->is_open()) {
            $channel->wait();
        }
    }
}