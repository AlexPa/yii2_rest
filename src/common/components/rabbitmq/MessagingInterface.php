<?php

namespace common\components\rabbitmq;

interface MessagingInterface
{
    public function send(string $message, string $key): bool;

    public function receive(string $queue, string $key, \Closure $func): void;
}