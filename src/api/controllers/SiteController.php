<?php declare(strict_types=1);

namespace api\controllers;

use common\components\job\CreateTestFileJob;
use common\components\mailerexample\MailerInterface;
use common\components\rabbitmq\MessagingInterface;
use common\models\Statistic;
use Yii;
use yii\helpers\Json;
use yii\queue\amqp_interop\Queue;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class SiteController extends Controller
{
    private MailerInterface $mailer;
    private MessagingInterface $messaging;

    public function __construct($id, $module, MailerInterface $mailer, MessagingInterface $messaging, $config = [])
    {
        $this->mailer = $mailer;
        $this->messaging = $messaging;

        parent::__construct($id, $module, $config);
    }

    public function actionClick(string $campaign)
    {
        $clickId = Statistic::generateUiniqId();

        // Instead make click directly send to queue
        //Statistic::makeClick($campaign, $clickId);
        $this->messaging->send(
            Json::encode(
                [
                    'click_id' => $clickId,
                    'ip' => Yii::$app->request->getRemoteIP(),
                    'campaign' => $campaign
                ]
            ),
            "site.make_click");


        $params = [
            'click_id' => $clickId
        ];
        $redirect = 'https://alfabank.ru?' . http_build_query($params);
        return $this->response->redirect($redirect);
    }

    public function actionTestQueue()
    {
        $filename = Yii::getAlias('@api/testFile.txt');
        /**
         * @var $queue Queue
         */
        $queue = Yii::$app->queue;
        $id = $queue->push(new CreateTestFileJob(['filename' => $filename]));
        if (file_exists($filename)) {
            return file_get_contents($filename);
        }
        return 'File not yet exists.';
    }

    public function actionIndex()
    {
        return "This is DI test. Mail sended by " . $this->mailer->send();
    }

    /*public function actionGenerate()
    {
        $files = [
            Yii::getAlias('@api/modules/v1/models/definitions'),
            Yii::getAlias('@api/modules/v1/controllers/AuthController.php'),
            Yii::getAlias('@api/modules/v1/controllers/PostController.php')
        ];

        $openApi = \OpenApi\Generator::scan($files);
        $file = Yii::getAlias('@api/swagger.json');
        $handle = fopen($file, 'wb');
        fwrite($handle, $openApi->toJson());
        fclose($handle);
        return true;
    }*/

    public function actionError()
    {
        if (($exception = Yii::$app->getErrorHandler()->exception) === null) {
            $exception = new NotFoundHttpException(Yii::t('yii', 'Page not found.'));
        }

        if ($exception instanceof \HttpException) {
            Yii::$app->response->setStatusCode($exception->getCode());
        } else {
            Yii::$app->response->setStatusCode(500);
        }

        return $this->asJson(['error' => $exception->getMessage(), 'code' => $exception->getCode()]);
    }
}