<?php

namespace api\modules\v1\controllers;

use common\models\UserToken;
use frontend\modules\user\models\LoginForm;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\rest\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

class AuthController extends Controller
{
    public function behaviors()
    {
        $behaviors = [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'token' => ['POST'],
                ],
            ],
        ];
        return ArrayHelper::merge($behaviors, parent::behaviors());
    }

    /**
     * @return array|string
     * @throws ForbiddenHttpException
     * @throws \Throwable
     * @throws \yii\base\Exception
     * @throws \yii\db\StaleObjectException
     */
    public function actionToken()
    {
        \Yii::$app->response->format = Response::FORMAT_RAW;
        \Yii::$app->response->headers->set("Access-Control-Allow-Origin", "*");

        $model = new LoginForm();
        $model->identity = \Yii::$app->request->post('email', null);
        $model->password = \Yii::$app->request->post('password', null);
        $valid = $model->validate();
        if ($valid === false) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            \Yii::$app->response->setStatusCode(422, "Unprocessable Content");
            return ['errors' => $model->getErrors()];
        }

        $token = UserToken::create(
            $model->getUser()->getId(),
            UserToken::TYPE_LOGIN_PASS,
            60
        );

        $user = UserToken::use($token, UserToken::TYPE_LOGIN_PASS);

        if ($user === null) {
            throw new ForbiddenHttpException();
        }

        return $user->access_token;
    }
}