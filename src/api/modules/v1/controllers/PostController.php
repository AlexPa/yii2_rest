<?php

namespace api\modules\v1\controllers;

use api\modules\v1\actions\DeleteAction;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\ActiveController;
use yii\rest\Serializer;

class PostController extends ActiveController
{
    public $modelClass = 'api\modules\v1\resources\Post';

    public $serializer = [
        'class' => Serializer::class,
        'collectionEnvelope' => 'data',
    ];

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::class,
        ];
        $behaviors['bearerAuth'] = [
            'class' => HttpBearerAuth::class,
        ];
        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();
        $actions['index']['pagination']['defaultPageSize'] = 10;
        $actions['delete']['class'] = DeleteAction::class;

        return $actions;
    }
}