<?php

namespace api\modules\v1\resources;

use yii\web\Linkable;

class Post extends \common\models\Post implements Linkable
{

    public function getLinks()
    {
        return [];
    }
}