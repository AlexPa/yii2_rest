<?php

namespace api\modules\v1\actions;

use Yii;
use yii\db\ActiveRecordInterface;
use yii\web\ServerErrorHttpException;

class DeleteAction extends \yii\rest\DeleteAction
{

    public function run($id)
    {
        $model = $this->findModel($id);
        if ($model === null) {
            Yii::$app->getResponse()->setStatusCode(204);
            return true;
        }
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id, $model);
        }

        if ($model->delete() === false) {
            throw new ServerErrorHttpException('Failed to delete the object for unknown reason.');
        }

        Yii::$app->getResponse()->setStatusCode(204);
    }

    public function findModel($id)
    {
        if ($this->findModel !== null) {
            return call_user_func($this->findModel, $id, $this);
        }

        /* @var $modelClass ActiveRecordInterface */
        $modelClass = $this->modelClass;
        $keys = $modelClass::primaryKey();
        if (count($keys) > 1) {
            $values = explode(',', $id);
            if (count($keys) === count($values)) {
                $model = $modelClass::findOne(array_combine($keys, $values));
            }
        } elseif ($id !== null) {
            $model = $modelClass::findOne($id);
        }

        if (isset($model)) {
            return $model;
        }

        return null;
    }
}