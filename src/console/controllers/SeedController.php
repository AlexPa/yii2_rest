<?php

namespace console\controllers;

use common\models\Post;
use common\models\Statistic;
use common\models\User;
use yii\console\Controller;
use yii\db\Expression;

class SeedController extends Controller
{
    public static function seed()
    {
        echo '123ppp';
    }

    public function actionIndex()
    {
        $faker = \Faker\Factory::create();


        $user = User::find()->one();
        for ($i = 1; $i <= 11; $i++) {
            $post = new Post();
            $post->user_id = $user->id;
            //$post->title = $faker->sentence();
            $post->title = 'Заголовок номер ' . $i;
            $post->body = $faker->text();
            if ($post->save()) {
                echo $post->id . " saved. " . PHP_EOL;
            }
        }
    }

    public function actionStatistic()
    {
        for ($i = 1; $i <= 100000; $i++) {
            $m = new Statistic();
            $m->click_id = Statistic::generateUiniqId();
            $m->user_id = 1;
            $m->date = new Expression('NOW()');
            $m->event_type = 1;
            $m->ip = '127.0.0.1';
            $m->save(false);
            echo $m->id . PHP_EOL;
        }
    }

    public function actionBatchStatistic()
    {
        $connection = \Yii::$app->db;
        $connection->enableLogging = false;
        $connection->enableProfiling = false;
        $connection->createCommand("SET unique_checks=0;")->execute();
        $start = time();
        $columns = [
            'click_id', 'user_id', 'date', 'event_type', 'ip'
        ];
        $iStep = 1000;
        $added = 0;
        for ($j = 0; $j < 100000; $j++) {
            $rows = null;
            $rows = [];
            for ($i = 0; $i < $iStep; $i++) {
                $rows[] = [
                    'click_id' => Statistic::generateNanoId(),
                    'user_id' => 1,
                    'date' => new Expression('NOW()'),
                    'event_type' => 1,
                    'ip' => '192.168.1.2'
                ];
            }
            $cmd = $connection->createCommand()
                ->batchInsert(Statistic::tableName(), $columns, $rows);

            $cmd->execute();

            $added += $iStep;
            echo $added . " - " . memory_get_usage() . PHP_EOL;
        }
        $connection->createCommand("SET unique_checks=1;")->execute();
        echo 'Done: ' . (time() - $start) . PHP_EOL;
    }

}