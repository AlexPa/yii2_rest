<?php

namespace console\controllers;

use common\components\rabbitmq\MessagingInterface;
use common\models\Statistic;
use PhpAmqpLib\Message\AMQPMessage;
use yii\console\Controller;
use yii\helpers\Json;

class RabbitmqController extends Controller
{
    private MessagingInterface $messaging;

    public function __construct($id, $module, MessagingInterface $messaging, $config = [])
    {
        $this->messaging = $messaging;
        parent::__construct($id, $module, $config);
    }

    public function actionListenForClick()
    {
        $this->messaging->receive("yii2_queue", "site.make_click", function (AMQPMessage $msg) {
            echo $msg->getBody() . " - " . memory_get_usage() . PHP_EOL;
            $data = Json::decode($msg->getBody());
            if (isset($data['click_id']) && isset($data['campaign'])) {
                Statistic::makeClick($data['campaign'], $data['click_id'], $data['ip']);
            }
        });
    }
    /*public function actionTest()
    {
        try {
//            $connection = new AMQPStreamConnection(
//                'rat-01.rmq2.cloudamqp.com',
//                5672,
//                'amqieljk',
//                'gITOrGQi3REFsNcJd-8xc2ybdORmyx85',
//                'amqieljk');
            $connection = new AMQPStreamConnection(
                'rabbitmq',
                5672,
                'guest',
                'guest',
                '/');
            $channel = $connection->channel();

            $channel->queue_declare('yii2_queue', false, false, false, false);
            echo " [*] Waiting for messages. To exit press CTRL+C\n";

            $callback = function (AMQPMessage $msg) {
                echo ' [x] Received ', $msg->getRoutingKey() . $msg->getBody(), "\n";
            };

            $channel->basic_consume('yii2_queue', '', false, true, false, false, $callback);

            while ($channel->is_open()) {
                $channel->wait();
            }

        } catch (\Exception $e) {
            print_r($e->getMessage());
        }
    }*/
}