<?php

use yii\db\Migration;

/**
 * Class m230222_014535_m1
 */
class m230222_014535_m1 extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m230222_014535_m1 cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m230222_014535_m1 cannot be reverted.\n";

        return false;
    }
    */
}
