<?php
$config = yii\helpers\ArrayHelper::merge(
    require(YII_APP_BASE_PATH . '/common/config/base.php'),
    require(YII_APP_BASE_PATH . '/common/config/web.php'),
    require(YII_APP_BASE_PATH . '/api/config/base.php'),
    require(YII_APP_BASE_PATH . '/api/config/web.php'),
    [
        'components' => [
            'assetManager' => [
                'basePath' => YII_APP_BASE_PATH . '/api/web/assets'
            ],
            'db' => [
                'dsn' => env('TEST_DB_DSN'),
                'username' => env('TEST_DB_USERNAME'),
                'password' => env('TEST_DB_PASSWORD')
            ],
            'mailer' => [
                'useFileTransport' => true,
            ],
            'urlManager' => [
                'showScriptName' => false,
            ],
        ],
    ],
    [
        'homeUrl' => '/',
        'components' => [
            'request' => [
                'enableCsrfValidation' => false,
            ],
        ],
    ]
);
if (isset($config['modules']['debug'])) {
    unset($config['modules']['debug']);
    foreach ($config['bootstrap'] as $k => $v) {
        if ($v === 'debug') {
            unset($config['bootstrap'][$k]);
        }
    }
}

return $config;
