<?php


namespace Tests\Api;

use tests\fixtures\PostFixture;
use tests\fixtures\UserFixture;
use Tests\Support\ApiTester;

class PostsCest
{
    private string $token = "";

    public function _before(ApiTester $I)
    {
    }

    public function _fixtures()
    {
        return [
            'user' => [
                'class' => UserFixture::class,
                'dataFile' => __DIR__ . '/../fixtures/data/user.php'
            ],
            'post' => [
                'class' => PostFixture::class,
                'dataFile' => __DIR__ . '/../fixtures/data/post.php'
            ]
        ];
    }

    public function authTest(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/x-www-form-urlencoded');
        $I->sendPost('/auth/token', [
            'email' => 'webmaster@example.com',
            'password' => 'webmaster'
        ]);
        $I->seeResponseCodeIsSuccessful();
    }

    // tests

    public function getPostsTest(ApiTester $I)
    {
        $token = $this->getToken($I);
        $I->haveHttpHeader('Authorization', 'Bearer ' . $token);
        $I->haveHttpHeader('Accept', 'application/json');
        $I->sendGet('/posts');

        $I->seeResponseCodeIsSuccessful();
        $I->canSeeResponseContainsJson([
            'data' => [],
            '_links' => [],
            '_meta' => []
        ]);
        $I->seeHttpHeader("X-Pagination-Total-Count");
        $I->seeHttpHeader("X-Pagination-Page-Count");
        $I->seeHttpHeader("X-Pagination-Current-Page");
        $I->seeHttpHeader("X-Pagination-Per-Page");
        $I->seeHttpHeader("Content-Type", "application/json; charset=UTF-8");
    }

    private function getToken(ApiTester $I)
    {
        if ($this->token === "") {
            $I->haveHttpHeader('Content-Type', 'application/x-www-form-urlencoded');
            $I->sendPost('/auth/token', [
                'email' => 'webmaster@example.com',
                'password' => 'webmaster'
            ]);
            $I->seeResponseCodeIsSuccessful();
            $this->token = $I->grabResponse();
        }
        return $this->token;
    }

    public function createPostTest(ApiTester $I)
    {
        $token = $this->getToken($I);
        $I->haveHttpHeader('Authorization', 'Bearer ' . $token);
        $I->haveHttpHeader('Accept', 'application/json');
        $I->sendPost('/posts', [
            "title" => "title 1",
            "body" => "Some text 1 2 3",
            "user_id" => "1"
        ]);

        $I->seeResponseCodeIs(201);
        $I->seeHttpHeader("Content-Type", "application/json; charset=UTF-8");
    }
}
