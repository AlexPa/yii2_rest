<?php

return [
    [
        //'id' => 1,
        'username' => 'webmaster',
        'auth_key' => 'KaKz-Tt9Qkn3-dYC3_dPRlVN7PmenVUB',
        'password_hash' => '$2y$13$rOzu5Z3Ta.dvz6ENgkGLPOonVLfTVJuHnOesoD.pJBCSaxCpRiwFK',
        'access_token' => 'cZRzAunZVchubwd8f8lNhXOJGX-A2gBycLiOZqL2',
        'created_at' => '1392559490',
        'updated_at' => '1392559490',
        'email' => 'webmaster@example.com',
    ],
    [
        //'id' => 2,
        'username' => 'manager',
        'auth_key' => 'tUu1qHcde0diwUol3xeI-18MuHkkprQI',
        'password_hash' => '$2y$13$N785qekuqJzo2CsP7K0g/.KtWZ8SwZtqITdTPrHBFITYjX9WYnl5i',
        'access_token' => 'JJcEWMSq4IFxhEMUpYoJXx5ZNDW33t4OFS5tXSlP',
        'created_at' => '1392559490',
        'updated_at' => '1392559490',
        'email' => 'user1@example.org'
    ],
    [
        //'id' => 3,
        'username' => 'user',
        'auth_key' => 'tUu1qHcde0diwUol3xeI-18MuHkkprQI',
        'password_hash' => '$2y$13$tJKZTKUQ5DBFWNkkjd0goup.p8Tx5d9Mj/wWL6Vv8/Q038zk7g5.6',
        'access_token' => 'Q1M6dPrGpzBWOnGf2NbkEMLntSCDhchuVKDGOUWC',
        'created_at' => '1392559490',
        'updated_at' => '1392559490',
        'email' => 'user2@example.org'
    ],
];
